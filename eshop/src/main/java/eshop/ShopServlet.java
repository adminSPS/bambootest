package eshop;

import java.io.IOException;
import java.util.Random;

import javax.servlet.RequestDispatcher;
import javax.servlet.Servlet;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import eshop.model.DataManager;

public class ShopServlet extends HttpServlet implements Servlet
{
	private static final Logger LOGGER = Logger
	.getLogger(ShopServlet.class);
	
  private static final long serialVersionUID = 1L;
    
  private String dummpString = new String ("");
  
  private static String synchString = new String ("");
  
  private static String synchString2 = new String ("");
  
  
  private static Random rnd = new Random();
  int requestCount = 0;

  public void init(ServletConfig config)
    throws ServletException
  {
   
    super.init(config);
     try
    {
    //URL url = Loader.getResource("./log4j.properties");
    //PropertyConfigurator.configure(url);
    
    DataManager dataManager = new DataManager();
//    dataManager.setDbURL(config.getInitParameter("dbURL"));
//    dataManager.setDbUserName(config.getInitParameter("dbUserName"));
//    dataManager.setDbPassword(config.getInitParameter("dbPassword"));

    ServletContext context = config.getServletContext();
    context.setAttribute("base", config.getInitParameter("base"));
    context.setAttribute("imageURL", config.getInitParameter("imageURL"));
    context.setAttribute("dataManager", dataManager);
    
     // Class.forName(config.getInitParameter("jdbcDriver"));
    } catch (Exception e) {
    	LOGGER.error(e.getMessage(),e);
    }
  }

  private void processRequest (HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	  requestCount ++;
	  System.out.println("Servlet Requests count " + requestCount);
	  Cookie cookie = new Cookie("defaultUser","" + requestCount);
	  response.addCookie(cookie );
    String base = "/jsp/";
    String url = base + "index.jsp";
    String action = request.getParameter("action");
    if (action != null)
      if (action.equals("search"))
        url = base + "SearchOutcome.jsp";
      else if (action.equals("selectCatalog"))
        url = base + "SelectCatalog.jsp";
      else if (action.equals("bookDetails"))
        url = base + "BookDetails.jsp";
      else if (action.matches("(showCart|(add|update|delete)Item)"))
        url = base + "ShoppingCart.jsp";
      else if (action.equals("checkOut"))
        url = base + "Checkout.jsp";
      else if (action.equals("orderConfirmation"))
        url = base + "OrderConfirmation.jsp";

    RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher (url);
    

    requestDispatcher.forward(request, response);
}
  
  protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
  {
	  
	  processRequest(request,response);
	  
	

  }

private void doLock_1_2() {
	 


	synchronized (synchString) {
		
		  System.out.println("... synch ...");
		  try {			  			 
			  dummpString = new String (Thread.currentThread().getName());
			  System.out.println("Thread :" + dummpString + " :Servlet :" + this);
			  Thread.sleep(10000);
			  synchronized (synchString2) {
				  dummpString = new String (Thread.currentThread().getName());
				  System.out.println("Thread :" + dummpString + " : lock " + 2);
				  Thread.sleep(20000);
			  }
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	  }
	
}



  protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
  {
	  processRequest(request,response);


  }
  
  
  
  
  
  
  
  
  /*
 //		String userName = request.getParameter("userName");
//		if(userName != null && ! userRequests.containsKey(userName)) {
//			userRequests.put(userName, 0);
//		}
	//	
//		Cookie[] cookies = request.getCookies();
//		for(int i = 0 ; i < cookies.length; i ++) {
//			
//			if(cookies[i].getName() != null && userRequests.containsKey(cookies[i].getName())) {
//				Integer count = userRequests.get(cookies[i].getName());
//				cookies[i].setValue((count + 1)+"");
//			} else {
//				System.out.println("Request Count " + requestCount);
//			    Cookie cookie = new Cookie("defaultUser","" + 0);
//				response.addCookie(cookie );
//			}
//		    
//		}
		
			Cookie[] cookies = request.getCookies();
//			for(int i = 0 ; i < cookies.length; i ++) {
//				if(cookies[i].getName() !=) {
//					
//				}
//			}
			
//			if(requestCount == 0) {
//					Cookie cookie = new Cookie("defaultUser","" + requestCount);
//					response.addCookie(cookie );
//			}
   */
}