package eshop.beans;

public class CartItem
{
  private String author;
  private String title;
  private double price;
  private String bookID;
  private String quantity;

  public CartItem(Book book, int quantity)
  {
    this.bookID = book.getId();
    this.title = book.getTitle();
    this.author = book.getAuthor();
    this.price = book.getPrice();
    this.quantity = new Integer(quantity).toString();
  }

  public String getAuthor() {
    return this.author;
  }

  public void setAuthor(String author) {
    this.author = author;
  }

  public String getTitle() {
    return this.title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public double getPrice() {
    return this.price;
  }

  public void setPrice(double price) {
    this.price = price;
  }

  public String getBookID() {
    return this.bookID;
  }

  public void setBookId(String bookID) {
    this.bookID = bookID;
  }

  public String getQuantity() {
    return this.quantity;
  }

  public void setQuantity(String quantity) {
    this.quantity = quantity;
  }
}