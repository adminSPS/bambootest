package eshop.beans;

public class Book
{
  private String author;
  private String id;
  private double price;
  private String title;

  public String getAuthor()
  {
    return this.author;
  }

  public void setAuthor(String author) {
    this.author = author;
  }

  public String getId() {
    return this.id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public double getPrice() {
    return this.price;
  }

  public void setPrice(double price) {
    this.price = price;
  }

  public String getTitle() {
    return this.title;
  }

  public void setTitle(String title) {
    this.title = title;
  }
}