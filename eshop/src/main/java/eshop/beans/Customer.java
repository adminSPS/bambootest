package eshop.beans;

public class Customer
{
  private String contactName = "";
  private String deliveryAddress = "";
  private String ccName = "";
  private String ccNumber = "";
  private String ccExpiryDate = "";

  public String getContactName()
  {
    return this.contactName;
  }

  public void setContactName(String contactName) {
    this.contactName = contactName;
  }

  public String getDeliveryAddress() {
    return this.deliveryAddress;
  }

  public void setDeliveryAddress(String deliveryAddress) {
    this.deliveryAddress = deliveryAddress;
  }

  public String getCcName() {
    return this.ccName;
  }

  public void setCcName(String ccName) {
    this.ccName = ccName;
  }

  public String getCcNumber() {
    return this.ccNumber;
  }

  public void setCcNumber(String ccNumber) {
    this.ccNumber = ccNumber;
  }

  public String getCcExpiryDate() {
    return this.ccExpiryDate;
  }

  public void setCcExpiryDate(String ccExpiryDate) {
    this.ccExpiryDate = ccExpiryDate;
  }
}