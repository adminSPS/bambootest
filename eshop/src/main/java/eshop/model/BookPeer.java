package eshop.model;

import eshop.beans.Book;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import org.apache.log4j.Logger;

public class BookPeer
{	private static final Logger LOGGER = Logger
	.getLogger(BookPeer.class);

  public BookPeer()
  {

  }

  public static ArrayList searchBooks(DataManager dataManager, String keyword)
  {
    ArrayList books = new ArrayList();

    Connection connection = dataManager.getConnection();
    if (connection != null)
      try {
        Statement s = connection.createStatement();
        String sql = "select book_id, title, author, price from books where title like '%" + 
          keyword.trim() + "%'" + 
          " or author like '%" + keyword.trim() + "%'";
        try {
         
          ResultSet rs = s.executeQuery(sql);
         
          try {
            while (rs.next())
            {
              Book book = new Book();
              book.setId(rs.getString(1));
              book.setTitle(rs.getString(2));
              book.setAuthor(rs.getString(3));
              book.setPrice(rs.getDouble(4));
              books.add(book);
            }
          } finally {
            rs.close();
          }
        } finally {
          s.close();
        }
      } catch (SQLException e) {
    	  LOGGER.error("Could not search for books:" , e);
        dataManager.putConnection(connection); } finally { dataManager.putConnection(connection);
      }

    return books;
  }

  public static ArrayList getBooksByCategory(DataManager dataManager, String categoryId)
  {
    
    ArrayList books = new ArrayList();
    Connection connection = dataManager.getConnection();
    if (connection != null)
      try {
        Statement s = connection.createStatement();
        String sql = "select book_id, title, author, price from books where category_id=" + 
          categoryId;
        try {
           
          ResultSet rs = s.executeQuery(sql);
        
          try {
            while (rs.next()) {
              Book book = new Book();
              book.setId(rs.getString(1));
              book.setTitle(rs.getString(2));
              book.setAuthor(rs.getString(3));
              book.setPrice(rs.getDouble(4));
              books.add(book);
            }
          } finally {
            rs.close();
          }
        } finally {
          s.close();
        }
      } catch (SQLException e) {        
        LOGGER.error("Could not get books." , e);
        dataManager.putConnection(connection); } finally { dataManager.putConnection(connection);
      }

    return books;
  }

  public static Book getBookById(DataManager dataManager, String bookID)
  {


    Book book = null;
    Connection connection = dataManager.getConnection();
    if (connection != null)
      try {
        Statement s = connection.createStatement();
        String sql = "select book_id, title, author, price from books where book_id=" + 
          bookID;
        try {
          ResultSet rs = s.executeQuery(sql);
          if (rs.next()) {
            book = new Book();
            book.setId(rs.getString(1));
            book.setTitle(rs.getString(2));
            book.setAuthor(rs.getString(3));
            book.setPrice(rs.getDouble(4));
          }
        } finally {
          s.close();
        }
      } catch (SQLException e) {
        LOGGER.error("Could not get book." , e);
        dataManager.putConnection(connection); } finally { dataManager.putConnection(connection);
      }

    return book;
  }

 
}