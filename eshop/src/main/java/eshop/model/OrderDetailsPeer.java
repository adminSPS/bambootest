package eshop.model;

import eshop.beans.CartItem;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Enumeration;
import java.util.Hashtable;

public class OrderDetailsPeer
{
  public static void insertOrderDetails(Statement stmt, long orderId, Hashtable shoppingCart)
    throws SQLException
  {
    System.gc();

    Enumeration enumList = shoppingCart.elements();
    while (enumList.hasMoreElements()) {
      CartItem item = (CartItem)enumList.nextElement();
      String sql = "insert into order_details (order_id, book_id, quantity, price, title, author) values ('" + 
        orderId + "','" + 
        item.getBookID() + "','" + item.getQuantity() + "','" + 
        item.getPrice() + "','" + item.getTitle() + "','" + 
        item.getAuthor() + "')";
      stmt.executeUpdate(sql);
    }
  }
}