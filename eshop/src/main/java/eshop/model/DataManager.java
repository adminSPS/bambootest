package eshop.model;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;

import org.apache.log4j.Logger;

import eshop.beans.Book;
import eshop.beans.Category;
import eshop.beans.Customer;

public class DataManager {
	private static final Logger LOGGER = Logger.getLogger(DataManager.class);
	
	int i = 0;
	private String dbURL = "";
	private String dbUserName = "";
	private String dbPassword = "";

	public DataManager() {

	}

	public void setDbURL(String dbURL) {
		this.dbURL = dbURL;
	}

	public String getDbURL() {
		return this.dbURL;
	}

	public void setDbUserName(String dbUserName) {
		this.dbUserName = dbUserName;
	}

	public String getDbUserName() {
		return this.dbUserName;
	}

	public void setDbPassword(String dbPassword) {
		this.dbPassword = dbPassword;
	}

	public String getDbPassword() {
		return this.dbPassword;
	}

	public Connection getConnection() {
		Connection conn = null;
		try {
			
			System.out.println("New connection : " + i);
			 i+=1;
			Context ctx;

			ctx = new InitialContext();
			Context envctx = (Context) ctx.lookup("java:comp/env");//ldap://server
			DataSource ds = (DataSource) envctx.lookup("jdbc/eShopDB");

			conn = ds.getConnection();

		} catch (Exception e) {
			LOGGER.error("Could not connect to DB: ", e);

		}
		return conn;
	}

	public void putConnection(Connection conn) {
		if (conn != null)
			try {
				conn.close();
			} catch (SQLException localSQLException) {
			}
	}

	public String getCategoryName(String categoryID) {
		Category category = CategoryPeer.getCategoryById(this, categoryID);
		return ((category == null) ? null : category.getName());
	}

	public Hashtable getCategories() {
		return CategoryPeer.getAllCategories(this);
	}

	public Enumeration getCatIDs() {
		return CategoryPeer.getAllCategories(this).keys();
	}

	public ArrayList getSearchResults(String keyword) {
		return BookPeer.searchBooks(this, keyword);
	}

	public ArrayList getBooksInCategory(String categoryID) {
		return BookPeer.getBooksByCategory(this, categoryID);
	}

	public Book getBookDetails(String bookID) {
		return BookPeer.getBookById(this, bookID);
	}

	public long insertOrder(Customer customer, Hashtable shoppingCart) {
		long returnValue = 0L;
		long orderId = System.currentTimeMillis();
		Connection connection = getConnection();
		if (connection != null) {
			Statement stmt = null;
			try {
				connection.setAutoCommit(false);
				stmt = connection.createStatement();
				try {
					OrderPeer.insertOrder(stmt, orderId, customer);
					OrderDetailsPeer.insertOrderDetails(stmt, orderId, shoppingCart);
					try {
						stmt.close();
					} finally {
						stmt = null;
					}
					connection.commit();
					returnValue = orderId;
				} catch (SQLException e) {
					LOGGER.error("Could not insert order: ", e);

					try {
						connection.rollback();
					} catch (SQLException ee) {
					}
				}
			} catch (SQLException e) {
				LOGGER.error("Could not insert order: ", e);
				// System.out.println("Could not insert order: " +
				// e.getMessage());
			} finally {
				if (stmt != null) {
					try {
						stmt.close();
					} catch (SQLException e) {
					}
				}
				putConnection(connection);
			}
		}
		return returnValue;
	}

	public void update(String bookId) {

	}

}